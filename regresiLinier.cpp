# include <stdio.h>
# include <stdlib.h>
# include <cstdlib>
#include <conio.h>
# include <string.h>
# include <math.h>

int digitTerpanjang(float data, int ket){
	char hasil2[20];
	int terpanjang = 1;
	sprintf(hasil2,"%f",data);

	if((strlen(hasil2) - 8 + ket) > 4){
		terpanjang = (strlen(hasil2)/6) +1;

	//!	digunakan untuk debug 
	//*	printf("panjangnya adalah : %d\n",strlen(hasil2)); 

	}
	return terpanjang;
}

void kalimatAwal(char kata[5],int tab){
	printf("|%s",kata);
	for (int j=0;j<tab;j++){
		printf("\t");
	}
}

void kalimatIterasi(int data, int tab){
	printf("|%d",data);
	for (int j=0;j<tab;j++){
		printf("\t");
	}
}

void kalimatData(float data, int tab, int ket){
	char hasil2[10];
	sprintf(hasil2,"%f",data);
	switch (ket){
		case 1:
			printf("|%.1f",data);
			break;
		case 2:
			printf("|%.2f",data);
			break;
		case 3:
			printf("|%.3f",data);
			break;
		case 4:
			printf("|%.4f",data);
			break;
		case 5:
			printf("|%.5f",data);
			break;
		case 6:
			printf("|%.6f",data);
			break;
	}
	if(strlen(hasil2) - 6 + ket >= 6){
		tab -= ((strlen(hasil2) - (6-ket-1))/8
		);
	}
	for (int j=0;j<tab;j++){
		printf("\t");	
	}
}

int main(){
	int N;
	int ket;
	int teliti;
	printf("Berapa kali anda melakukan pengamatan: ");
    scanf("%d",&N);
	printf("Masukkan jumlah angka dibelakang koma yang anda inginkan (max: 6) :");
	scanf("%d",&ket);
	teliti = ket;
	float dataX[N];
	float dataY[N];
	
	int i,j,k;
	printf("masukkan data hasil penelitian anda \n");
	
//todo	memasukkan data pengamatan X
	
    for (i=1;i<N+1;i++){
    	printf("data X ke-%i : ",i);
        scanf("%f",&dataX[i-1]);
        printf("data Y ke-%d : ",i);
        scanf("%f",&dataY[i-1]);
    }
    
//todo	proses regresi linear
	
	float dataXX[N];
	float dataXY[N];
	float dataYY[N];
	
	for (i=1;i<N+1;i++){
		dataXX[i-1] = dataX[i-1]*dataX[i-1];
		dataXY[i-1] = dataX[i-1]*dataY[i-1];
		dataYY[i-1] = dataY[i-1]*dataY[i-1];
	}
	
//todo	mencari nilai sigma
	float sigma[] = {0,0,0,0,0};
	
	for (i=1;i<N+1;i++){
		sigma[0] += dataX[i-1];
		sigma[1] += dataY[i-1];
		sigma[2] += dataXX[i-1];
		sigma[3] += dataYY[i-1];
		sigma[4] += dataXY[i-1];
	}
//todo mencari jumlah tab untuk baris awal
//todo	mencari jumlah digit terbanyak pada setiap kolom data
	char hasil[10];
	int jumlahTab[6] = {1,1,1,1,1,1};

//todo	 mencari panjang digit N
	sprintf(hasil,"%d",N);
	if (strlen(hasil) > 6){
		jumlahTab[0] = strlen(hasil)/6;
	}

//todo	 mencari digit terpanjang dataX, dataY, dataXX, dataYY dan dataXY
	for (i=1;i<N+1;i++){
		if (jumlahTab[1] < digitTerpanjang(dataX[i-1],ket)){
			jumlahTab[1] = digitTerpanjang(dataX[i-1],ket);
		}
		if (jumlahTab[2] < digitTerpanjang(dataY[i-1],ket)){
			jumlahTab[2] = digitTerpanjang(dataY[i-1],ket);
		}
		if (jumlahTab[3] < digitTerpanjang(dataXX[i-1],ket)){
			jumlahTab[3] = digitTerpanjang(dataXX[i-1],ket);
		}
		if (jumlahTab[4] < digitTerpanjang(dataYY[i-1],ket)){
			jumlahTab[4] = digitTerpanjang(dataYY[i-1],ket);
		}
		if (jumlahTab[5] < digitTerpanjang(dataXY[i-1],ket)){
			jumlahTab[5] = digitTerpanjang(dataXY[i-1],ket);
		}

	}

	//! fungsi berikut digunakan untuk debug

	// for (i=0;i<6;i++){
	// 	printf("%d\n",jumlahTab[i]);
	// }
	
	printf("\n\n");

//todo	membuat kalimat awal

	kalimatAwal("NO",jumlahTab[0]);
	kalimatAwal("X",jumlahTab[1]);
	kalimatAwal("Y",jumlahTab[2]);
	kalimatAwal("X^2",jumlahTab[3]);
	kalimatAwal("Y^2",jumlahTab[4]);
	kalimatAwal("XY",jumlahTab[5]);
	printf("|\n");


//todo	membuat kalimat untuk setiap baris data
	for (i=1;i<N+1;i++){
		kalimatIterasi(i,jumlahTab[0]);
		kalimatData(dataX[i-1],jumlahTab[1],ket);
		kalimatData(dataY[i-1],jumlahTab[2],ket);
		kalimatData(dataXX[i-1],jumlahTab[3],ket);
		kalimatData(dataYY[i-1],jumlahTab[4],ket);
		kalimatData(dataXY[i-1],jumlahTab[5],ket);
		printf("|\n");
	}

// todo bonus membuat string pembatas antara data dan sigma

	for (i=0;i<6;i++){
		printf("|");
		for (int j=0;j<jumlahTab[i]*8-1;j++){
			printf("-");
		}
	}

	printf("- +\n");

//todo	membuat sigma
	kalimatAwal("Sigma",jumlahTab[0]);
	kalimatData(sigma[0],jumlahTab[1],ket);
	kalimatData(sigma[1],jumlahTab[2],ket);
	kalimatData(sigma[2],jumlahTab[3],ket);
	kalimatData(sigma[3],jumlahTab[4],ket);
	kalimatData(sigma[4],jumlahTab[5],ket);
	printf("|\n\n\n");

//todo membuat hasil a dan b menggunakan rumus regresi linier

//todo membuat rumus a

	float a;

	// sigma [0] = sigma X
	// sigma [1] = sigma Y
	// sigma [2] = sigma X^2
	// sigma [3] = sigma Y^2
	// sigma [4] = sigma XY

	// 		sigma X^2 * sigma Y - sigma x * sigma XY
	// a = --------------------------------------------
	// 			N * sigma X^2 - (sigma X)^2

	a = ((sigma[2]*sigma[1]) - (sigma[0]*sigma[4]) )/((N*sigma[2]) - (sigma[0]*sigma[0]));

	switch (ket){
		case 1:
			printf("didapatkan a adalah : %.1f",a);
			break;
		case 2:
			printf("didapatkan a adalah : %.2f",a);
			break;
		case 3:
			printf("didapatkan a adalah : %.3f",a);
			break;
		case 4:
			printf("didapatkan a adalah : %.4f",a);
			break;
		case 5:
			printf("didapatkan a adalah : %.5f",a);
			break;
		case 6:
			printf("didapatkan a adalah : %.6f",a);
			break;
	}

	printf("\n");

//todo membuat fungsi untuk mencari b
	float b;

	// sigma [0] = sigma X
	// sigma [1] = sigma Y
	// sigma [2] = sigma X^2
	// sigma [3] = sigma Y^2
	// sigma [4] = sigma XY

	// 		N * sigma XY - sigma X * sigma Y
	// b = -------------------------------------
	// 		N * sigma X^2 - (sigma X)^2

	b = ((N*sigma[4]) - (sigma[0]*sigma[1]))/((N*sigma[2]) - (sigma[0]*sigma[0]));

	switch (ket){
		case 1:
			printf("didapatkan b adalah : %.1f",b);
			break;
		case 2:
			printf("didapatkan b adalah : %.2f",b);
			break;
		case 3:
			printf("didapatkan b adalah : %.3f",b);
			break;
		case 4:
			printf("didapatkan b adalah : %.4f",b);
			break;
		case 5:
			printf("didapatkan b adalah : %.5f",b);
			break;
		case 6:
			printf("didapatkan b adalah : %.6f",b);
			break;
	}
	printf("\n");

//todo mencari delta y untuk delta b
	float dy;

	// sigma [0] = sigma X
	// sigma [1] = sigma Y
	// sigma [2] = sigma X^2
	// sigma [3] = sigma Y^2
	// sigma [4] = sigma XY

	// rumus delta y cari aja di modul. panjang bat rumusnya

	dy = 1*(sigma[3] - ((sigma[2]*(sigma[1]*sigma[1]) - (2*sigma[0]*sigma[1]*sigma[4]) + (N*(sigma[4]*sigma[4]))))/((N*sigma[2]) - (sigma[0]*sigma[0])))/(N-2);
	dy = sqrt(dy);

	switch (ket){
		case 1:
			printf("didapatkan delta y adalah : %.1f",dy);
			break;
		case 2:
			printf("didapatkan delta y adalah : %.2f",dy);
			break;
		case 3:
			printf("didapatkan delta y adalah : %.3f",dy);
			break;
		case 4:
			printf("didapatkan delta y adalah : %.4f",dy);
			break;
		case 5:
			printf("didapatkan delta y adalah : %.5f",dy);
			break;
		case 6:
			printf("didapatkan delta y adalah : %.6f",dy);
			break;
	}
	printf("\n");

//todo mencari delta b dengan menggunakan delta y
	float db;

	// sigma [0] = sigma X
	// sigma [1] = sigma Y
	// sigma [2] = sigma X^2
	// sigma [3] = sigma Y^2
	// sigma [4] = sigma XY

	db = N/(N*sigma[2] - (sigma[0]*sigma[0]));
	db = sqrt(db);
	db = dy*db;

	switch (ket){
		case 1:
			printf("didapatkan delta b adalah : %.1f",db);
			break;
		case 2:
			printf("didapatkan delta b adalah : %.2f",db);
			break;
		case 3:
			printf("didapatkan delta b adalah : %.3f",db);
			break;
		case 4:
			printf("didapatkan delta b adalah : %.4f",db);
			break;
		case 5:
			printf("didapatkan delta b adalah : %.5f",db);
			break;
		case 6:
			printf("didapatkan delta b adalah : %.6f",db);
			break;
	}
	printf("\n");

//todo mencari TK (tingkat ketelitian)
	float tk;

	// 		 			delta b
	// tk = ( 1 - -------------------- )
	// 					  b

	tk = 1 - (db/b);
	tk *= 100;

	switch (ket){
		case 1:
			printf("didapatkan tingkat ketelitian adalah : %.1f",tk);
			break;
		case 2:
			printf("didapatkan tingkat ketelitian adalah : %.2f",tk);
			break;
		case 3:
			printf("didapatkan tingkat ketelitian adalah : %.3f",tk);
			break;
		case 4:
			printf("didapatkan tingkat ketelitian adalah : %.4f",tk);
			break;
		case 5:
			printf("didapatkan tingkat ketelitian adalah : %.5f",tk);
			break;
		case 6:
			printf("didapatkan tingkat ketelitian adalah : %.6f",tk);
			break;
	}
	printf("\n");
	getch();
    return 0;
}
